﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using NLog;

namespace FtpService.Core
{
    public class FtpServer : IDisposable
    {
        private bool _disposed;
        private bool _listening;

        private TcpListener _listener;
        private List<ClientConnection> _activeConnections;

        private readonly IPEndPoint _localEndPoint;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #region logging

        private static void LogInfo(string text)
        {
            if (Utils.LoggingIsEnabled())
            {
                Logger.Info(text);
            }
        }

        private static void LogDebug(string text)
        {
            if (Utils.LoggingIsEnabled())
            {
                Logger.Debug(text);
            }
        }

        private static void LogError(string text)
        {
            if (Utils.LoggingIsEnabled())
            {
                Logger.Error(text);
            }
        }

        private static void LogError(Exception e)
        {
            Logger.Error(e);
        }

        #endregion

        public FtpServer()
            : this(IPAddress.Any, Utils.GetPort())
        {
        }

        public FtpServer(IPAddress ipAddress, int port)
        {
            _localEndPoint = new IPEndPoint(ipAddress, port);
        }

        public void Start() {
            _localEndPoint.Port = Utils.GetPort();
            _listener = new TcpListener(_localEndPoint);

            LogInfo("#Version: 1.0");
            LogInfo("#Fields: c-ip c-port cs-username cs-method cs-uri-stem sc-status sc-bytes cs-bytes s-name s-port");

            _listening = true;
            _listener.Start();

            _activeConnections = new List<ClientConnection>();

            _listener.BeginAcceptTcpClient(HandleAcceptTcpClient, _listener);
        }

        public void Stop()
        {
            LogInfo("Stopping FtpServer");

            _listening = false;
            _listener.Stop();

            _listener = null;
        }

        private void HandleAcceptTcpClient(IAsyncResult result)
        {
            if (_listening)
            {
                _listener.BeginAcceptTcpClient(HandleAcceptTcpClient, _listener);

                var client = _listener.EndAcceptTcpClient(result);
                var connection = new ClientConnection(client);

                _activeConnections.Add(connection);

                ThreadPool.QueueUserWorkItem(connection.HandleClient, client);
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    Stop();

                    foreach (var conn in _activeConnections)
                    {
                        conn.Dispose();
                    }
                }
            }

            _disposed = true;
        }
    }
}
