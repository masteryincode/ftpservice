﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FtpService.Core
{
    class LogEntry
    {
        public string CIP { get; set; }
        public string CPort { get; set; }
        public string CSUsername { get; set; }
        public string CSMethod { get; set; }
        public string CSUriStem { get; set; }
        public string SCStatus { get; set; }
        public string SCBytes { get; set; }
        public string CSBytes { get; set; }
        public string SName { get; set; }
        public string SPort { get; set; }

        public override string ToString()
        {
            return
                $"{CIP} {CPort ?? "-"} {CSUsername} {CSMethod} {CSUriStem ?? "-"} {SCStatus} {SCBytes ?? "-"} {CSBytes ?? "-"} {SName ?? "-"} {SPort ?? "-"}";
        }
    }
}
