﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace FtpService.Core
{
    class UserManager
    {
        private static List<User> _users;

        private static void ReloadUsers() {
            _users = new List<User>();

            var serializer = new XmlSerializer(_users.GetType(), new XmlRootAttribute("Users"));
            var path = AppDomain.CurrentDomain.BaseDirectory + Path.DirectorySeparatorChar + "users.xml";

            if (File.Exists(path))
            {
                _users = serializer.Deserialize(new StreamReader(path)) as List<User>;
            }
            else
            {
                _users.Add(new User
                {
                    Username = "test",
                    Password = "test",
                    HomeDir = "C:\\FTP"
                });

                using (var w = new StreamWriter(path))
                {
                    serializer.Serialize(w, _users);
                }
            }
        }

        public static User Validate(string username, string password) {
            ReloadUsers();

            var user = (from u in _users where u.Username == username && u.Password == password select u).SingleOrDefault();

            return user;
        }
    }
}
