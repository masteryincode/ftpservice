﻿using System;
using System.ServiceProcess;
using FtpService.Core;
using NLog;

namespace FtpService
{
    public partial class FtpService : ServiceBase
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private FtpServer server;

        public FtpService()
        {
            InitializeComponent();
        }

        private static void LogInfo(string text)
        {
            if (Utils.LoggingIsEnabled())
            {
                logger.Info(text);
            }
        }

        private static void LogDebug(string text)
        {
            if (Utils.LoggingIsEnabled())
            {
                logger.Debug(text);
            }
        }

        private static void LogError(string text)
        {
            if (Utils.LoggingIsEnabled())
            {
                logger.Error(text);
            }
        }

        private static void LogError(Exception e)
        {
            logger.Error(e);
        }

        protected override void OnStart(string[] args)
        {
            LogInfo(AppDomain.CurrentDomain.BaseDirectory);
            LogInfo("The FTP Service is started");

            var port = Utils.GetPort();

            LogInfo($"Port = {port}");

            if (server == null) {
                server = new FtpServer();
            }

            server.Start();
        }

        protected override void OnStop()
        {
            server?.Stop();

            LogInfo("The FTP Service is stopped");
        }
    }
}
