﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace FtpService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        private static void Main(string[] args) {
            if (args.Length == 0) {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[] {
                    new FtpService()
                };
                ServiceBase.Run(ServicesToRun);
            } else if (args.Length == 1) {
                switch (args[0]) {
                    case @"-install":
                        Utils.InstallService();

                        break;

                    case @"-uninstall":
                        Utils.StopService();
                        Utils.UninstallService();

                        break;

                    case @"-start":
                        Utils.StartService();

                        break;

                    case @"-stop":
                        Utils.StopService();

                        break;

                    case @"-restart":
                        Utils.StopService();
                        Utils.StartService();

                        break;
                }
            }
        }
    }
}
