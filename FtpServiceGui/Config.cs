﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FtpServiceGui
{
    public class Config
    {
        public bool LoggingIsEnabled { get; set; }
        public string LogFile { get; set; }
        public int Port { get; set; }
        public bool UsersAreFound { get; set; }
        public List<User> Users { get; set; } = new List<User>();

        public override bool Equals(object obj) {
            var anotherConfig = obj as Config;

            if (anotherConfig == null)
            {
                return false;
            }

            return LoggingIsEnabled == anotherConfig.LoggingIsEnabled
                && LogFile.Equals(anotherConfig.LogFile)
                && Port == anotherConfig.Port
                && Utils.CombineHashCodes(Users.OrderBy(user => user.Username).Select(user => user.GetHashCode()))
                    == Utils.CombineHashCodes(anotherConfig.Users.OrderBy(user => user.Username).Select(user => user.GetHashCode()));
        }

        public override int GetHashCode() {
            return
                Utils.CombineHashCodes(new[] {
                    LoggingIsEnabled.GetHashCode(), LogFile.GetHashCode(), Port,
                    Utils.CombineHashCodes(Users.Select(user => user.GetHashCode()))
                });
        }
    }
}
