﻿namespace FtpServiceGui
{
    partial class ConfigForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.logFileLabel = new System.Windows.Forms.Label();
            this.portLabel = new System.Windows.Forms.Label();
            this.enableLogLabel = new System.Windows.Forms.Label();
            this.enableLogCheckBox = new System.Windows.Forms.CheckBox();
            this.logFileTextBox = new System.Windows.Forms.TextBox();
            this.portText = new System.Windows.Forms.MaskedTextBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.changeLogFileButton = new System.Windows.Forms.Button();
            this.manageUsersButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.logFileLabel, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.portLabel, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.enableLogLabel, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.enableLogCheckBox, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.logFileTextBox, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.portText, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.changeLogFileButton, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.manageUsersButton, 1, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 19F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(412, 177);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // logFileLabel
            // 
            this.logFileLabel.AutoSize = true;
            this.logFileLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.logFileLabel.Location = new System.Drawing.Point(3, 50);
            this.logFileLabel.Name = "logFileLabel";
            this.logFileLabel.Size = new System.Drawing.Size(77, 25);
            this.logFileLabel.TabIndex = 4;
            this.logFileLabel.Text = "Log file:";
            this.logFileLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // portLabel
            // 
            this.portLabel.AutoSize = true;
            this.portLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.portLabel.Location = new System.Drawing.Point(3, 0);
            this.portLabel.Name = "portLabel";
            this.portLabel.Size = new System.Drawing.Size(77, 25);
            this.portLabel.TabIndex = 0;
            this.portLabel.Text = "Port:";
            this.portLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // enableLogLabel
            // 
            this.enableLogLabel.AutoSize = true;
            this.enableLogLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.enableLogLabel.Location = new System.Drawing.Point(3, 25);
            this.enableLogLabel.Name = "enableLogLabel";
            this.enableLogLabel.Size = new System.Drawing.Size(77, 25);
            this.enableLogLabel.TabIndex = 2;
            this.enableLogLabel.Text = "Enable logging";
            this.enableLogLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // enableLogCheckBox
            // 
            this.enableLogCheckBox.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.enableLogCheckBox, 2);
            this.enableLogCheckBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.enableLogCheckBox.Location = new System.Drawing.Point(86, 28);
            this.enableLogCheckBox.Name = "enableLogCheckBox";
            this.enableLogCheckBox.Size = new System.Drawing.Size(323, 19);
            this.enableLogCheckBox.TabIndex = 3;
            this.enableLogCheckBox.UseVisualStyleBackColor = true;
            // 
            // logFileTextBox
            // 
            this.logFileTextBox.Dock = System.Windows.Forms.DockStyle.Left;
            this.logFileTextBox.Location = new System.Drawing.Point(86, 53);
            this.logFileTextBox.Name = "logFileTextBox";
            this.logFileTextBox.Size = new System.Drawing.Size(257, 20);
            this.logFileTextBox.TabIndex = 5;
            // 
            // portText
            // 
            this.portText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.portText.Location = new System.Drawing.Point(86, 3);
            this.portText.Mask = "00000";
            this.portText.Name = "portText";
            this.portText.Size = new System.Drawing.Size(257, 20);
            this.portText.TabIndex = 1;
            this.portText.ValidatingType = typeof(int);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel1.SetColumnSpan(this.tableLayoutPanel2, 2);
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.91641F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 51.08359F));
            this.tableLayoutPanel2.Controls.Add(this.okButton, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.cancelButton, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(86, 109);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(323, 65);
            this.tableLayoutPanel2.TabIndex = 6;
            // 
            // okButton
            // 
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.okButton.Location = new System.Drawing.Point(3, 39);
            this.okButton.MaximumSize = new System.Drawing.Size(0, 23);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(151, 23);
            this.okButton.TabIndex = 0;
            this.okButton.Text = "Ok";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.cancelButton.Location = new System.Drawing.Point(160, 39);
            this.cancelButton.MaximumSize = new System.Drawing.Size(0, 23);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(160, 23);
            this.cancelButton.TabIndex = 1;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // changeLogFileButton
            // 
            this.changeLogFileButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.changeLogFileButton.Location = new System.Drawing.Point(347, 51);
            this.changeLogFileButton.Margin = new System.Windows.Forms.Padding(1);
            this.changeLogFileButton.MaximumSize = new System.Drawing.Size(25, 0);
            this.changeLogFileButton.Name = "changeLogFileButton";
            this.changeLogFileButton.Size = new System.Drawing.Size(25, 23);
            this.changeLogFileButton.TabIndex = 7;
            this.changeLogFileButton.Text = "...";
            this.changeLogFileButton.UseVisualStyleBackColor = true;
            this.changeLogFileButton.Click += new System.EventHandler(this.changeLogFileButton_Click);
            // 
            // manageUsersButton
            // 
            this.manageUsersButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.manageUsersButton.Location = new System.Drawing.Point(86, 78);
            this.manageUsersButton.Name = "manageUsersButton";
            this.manageUsersButton.Size = new System.Drawing.Size(257, 25);
            this.manageUsersButton.TabIndex = 8;
            this.manageUsersButton.Text = "User management";
            this.manageUsersButton.UseVisualStyleBackColor = true;
            this.manageUsersButton.Click += new System.EventHandler(this.manageUsersButton_Click);
            // 
            // ConfigForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(412, 177);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "ConfigForm";
            this.Text = "Options";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label logFileLabel;
        private System.Windows.Forms.Label portLabel;
        private System.Windows.Forms.Label enableLogLabel;
        private System.Windows.Forms.CheckBox enableLogCheckBox;
        private System.Windows.Forms.TextBox logFileTextBox;
        private System.Windows.Forms.MaskedTextBox portText;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button changeLogFileButton;
        private System.Windows.Forms.Button manageUsersButton;
    }
}

