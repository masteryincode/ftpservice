﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using FtpServiceGui.Properties;

namespace FtpServiceGui
{
    public partial class ConfigForm : Form {
        private Config _currentConfig;
        private readonly UserManagementForm _userManagementForm = new UserManagementForm();

        public ConfigForm()
        {
            InitializeComponent();
            Icon = Resources.app2;
        }

        public void Fill(Config config) {
            _currentConfig = new Config {
                Port = config.Port,
                LoggingIsEnabled = config.LoggingIsEnabled,
                LogFile = config.LogFile,
                Users = new List<User>(config.Users)
            };

            portText.Text = config.Port.ToString();
            enableLogCheckBox.Checked = config.LoggingIsEnabled;
            logFileTextBox.Text = config.LogFile;

            manageUsersButton.Enabled = config.UsersAreFound;

            manageUsersButton.Text = manageUsersButton.Enabled == false
                ? @"User management (users.xml is not found)" : @"User management";
        }

        public Config Collect()
        {
            return new Config {
                Port = int.Parse(portText.Text),
                LoggingIsEnabled = enableLogCheckBox.Checked,
                LogFile = logFileTextBox.Text,
                Users = _currentConfig.Users
            };
        }

        private void changeLogFileButton_Click(object sender, EventArgs e)
        {
            var dialog = new OpenFileDialog {
                Filter = @"log files (*.log)|*.log|All files (*.*)|*.*",
                FilterIndex = 2,
                RestoreDirectory = true
            };

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                logFileTextBox.Text = dialog.FileName;
            }
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            if (!_currentConfig.Port.ToString().Equals(portText.Text)
                || _currentConfig.LoggingIsEnabled != enableLogCheckBox.Checked
                || !_currentConfig.LogFile.Equals(logFileTextBox.Text)) {
                MessageBox.Show("The service will be restarted");
            }
        }

        private void manageUsersButton_Click(object sender, EventArgs e) {
            _userManagementForm.Fill(_currentConfig.Users);
            _userManagementForm.ShowDialog();
            _currentConfig.Users.Clear();

            var collected = _userManagementForm.Collect();
            _currentConfig.Users.AddRange(collected);
        }
    }
}
