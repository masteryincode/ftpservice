﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

namespace FtpServiceGui
{
    class FtpServiceGuiApplicationContext : ApplicationContext
    {
        private const string LoggerConfig = "NLog.config";
        private const string ServiceExe = "FtpService.exe";
        private const string UsersConfig = "users.xml";

        private readonly NotifyIcon _notifyIcon;
        private readonly ConfigForm _configForm;

        public FtpServiceGuiApplicationContext()
        {
            _configForm = new ConfigForm();

            _notifyIcon = new NotifyIcon()
            {
                Icon = Properties.Resources.app2,
                ContextMenu = new ContextMenu(new[] {
                    new MenuItem("Install Service", (sender, args) => {
                        RunService("-install");
                    }),
                    new MenuItem("Uninstall Service", (sender, args) => {
                        RunService("-uninstall");
                    }),
                    new MenuItem("Start Service", (sender, args) => {
                        RunService("-start");
                    }),
                    new MenuItem("Stop Service", (sender, args) => {
                        RunService("-stop");
                    }),
                    new MenuItem("-"),
                    new MenuItem("Options...", (sender, args) => {
                        if (_configForm.Visible)
                        {
                            return;
                        }

                        if (!File.Exists(LoggerConfig)) {
                            MessageBox.Show(@"Could not found the logger config file.");

                            return;
                        }

                        var config = LoadConfig();

                        _configForm.Fill(config);

                        if (_configForm.ShowDialog() != DialogResult.OK) {
                            return;
                        }

                        var newConfig = _configForm.Collect();

                        if (config.Equals(newConfig)) {
                            return;
                        }

                        SaveConfig(_configForm.Collect());
                        RunService("-restart");
                    }),
                    new MenuItem("-"),
                    new MenuItem("Exit", (sender, args) => {
                        _notifyIcon.Visible = false;

                        Application.Exit();
                    })
                }),
                Visible = true
            };
        }

        private static void RunService(string arg) {
            var info = new ProcessStartInfo(ServiceExe) {
                Arguments = arg,
                UseShellExecute = true,
                Verb = "runas"
            };

            Process.Start(info);
        }

        private static Config LoadConfig() {
            var result = new Config();
            var loggerConfigXml = new XmlDocument();
            var loggerConfigXmlPath = AppDomain.CurrentDomain.BaseDirectory + Path.AltDirectorySeparatorChar + LoggerConfig;

            loggerConfigXml.Load(loggerConfigXmlPath);

            var targets = loggerConfigXml.GetElementsByTagName("target");

            if (targets.Count > 0) {
                var xmlAttributeCollection = targets[0].Attributes;

                if (xmlAttributeCollection != null) {
                    result.LogFile = xmlAttributeCollection["fileName"].Value ?? "";
                }
            }

            var serviceExePath = AppDomain.CurrentDomain.BaseDirectory + Path.AltDirectorySeparatorChar + ServiceExe;
            var serviceConfig = ConfigurationManager.OpenExeConfiguration(serviceExePath);
            var serviceAppSettings = serviceConfig.AppSettings;
            var port = serviceAppSettings.Settings["Port"].Value;
            int portValue;

            int.TryParse(port, out portValue);

            result.Port = portValue;

            var loggingIsEnabled = serviceAppSettings.Settings["EnableLogging"].Value;
            var loggingIsEnabledValue = true;

            bool.TryParse(loggingIsEnabled, out loggingIsEnabledValue);

            result.LoggingIsEnabled = loggingIsEnabledValue;

            var usersConfigPath = AppDomain.CurrentDomain.BaseDirectory + Path.AltDirectorySeparatorChar + UsersConfig;
            var users = new List<User>();
            var serializer = new XmlSerializer(users.GetType(), new XmlRootAttribute("Users"));
            var usersAreFound = false;

            if (File.Exists(usersConfigPath)) {
                using (var r = new StreamReader(usersConfigPath)) {
                    users = serializer.Deserialize(r) as List<User>;
                }

                usersAreFound = true;
            }

            result.UsersAreFound = usersAreFound;

            if (users != null) {
                result.Users.AddRange(users);
            }

            return result;
        }

        private static void SaveConfig(Config config)
        {
            var loggerConfigXml = new XmlDocument();
            var loggerConfigXmlPath = AppDomain.CurrentDomain.BaseDirectory + Path.AltDirectorySeparatorChar + LoggerConfig;

            loggerConfigXml.Load(loggerConfigXmlPath);

            var targets = loggerConfigXml.GetElementsByTagName("target");

            if (targets.Count > 0) {
                var xmlAttributeCollection = targets[0].Attributes;

                if (xmlAttributeCollection != null) {
                    xmlAttributeCollection["fileName"].Value = config.LogFile;
                }

                loggerConfigXml.Save(loggerConfigXmlPath);
            }

            var serviceExePath = AppDomain.CurrentDomain.BaseDirectory + Path.AltDirectorySeparatorChar + ServiceExe;
            var serviceConfig = ConfigurationManager.OpenExeConfiguration(serviceExePath);
            var serviceAppSettings = serviceConfig.AppSettings;

            serviceAppSettings.Settings["Port"].Value = config.Port.ToString();
            serviceAppSettings.Settings["EnableLogging"].Value = config.LoggingIsEnabled.ToString();

            serviceConfig.Save();

            var usersConfigPath = AppDomain.CurrentDomain.BaseDirectory + Path.AltDirectorySeparatorChar + UsersConfig;
            var serializer = new XmlSerializer(config.Users.GetType(), new XmlRootAttribute("Users"));

            if (File.Exists(usersConfigPath)) {
                using (var w = new StreamWriter(usersConfigPath)) {
                    serializer.Serialize(w, config.Users);
                }
            }
        }
    }
}
