﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace FtpServiceGui
{
    [Serializable]
    public class User
    {
        [XmlAttribute("username")]
        public string Username { get; set; }

        [XmlAttribute("password")]
        public string Password { get; set; }

        [XmlAttribute("homedir")]
        public string HomeDir { get; set; }

        public override bool Equals(object obj) {
            var anotherUser = obj as User;

            if (anotherUser == null) {
                return false;
            }

            return Username.Equals(anotherUser.Username)
                   && Password.Equals(anotherUser.Password)
                   && HomeDir.Equals(anotherUser.HomeDir);
        }

        public override int GetHashCode() {
            return Utils.CombineHashCodes(new []{ Username.GetHashCode(), Password.GetHashCode(), HomeDir.GetHashCode() });
        }
    }
}
