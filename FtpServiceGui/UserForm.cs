﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using FtpServiceGui.Properties;

namespace FtpServiceGui
{
    public partial class UserForm : Form {
        public enum Mode {
            Add,
            Edit,
            Delete
        };

        private readonly Dictionary<string, User> _usersDictionary;
        private readonly Mode _mode;
        private User _userData = new User();

        private string ValidateName(string name) {
            if (name.Length == 0) {
                return @"The name is empty";
            }

            switch (_mode) {
                case Mode.Add:
                    return _usersDictionary.ContainsKey(name) ? $@"The user '{name}' already exists" : string.Empty;
                case Mode.Edit:
                    return (!name.Equals(_userData.Username) && _usersDictionary.ContainsKey(name))
                        ? $@"The user '{name}' already exists"
                        : string.Empty;
                case Mode.Delete:
                    return string.Empty;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private static string ValidateHomeDir(string path) {
            if (path.Length == 0) {
                return @"The user's home directory path is empty";
            }

            return !Directory.Exists(path) ? $@"The directory {path} doesn't exists" : string.Empty;
        }

        private void ValidateFields() {
            validationLabel.Text = ValidateName(userNameTextBox.Text.Trim());

            if (validationLabel.Text.Equals(string.Empty))
            {
                validationLabel.Text = ValidateHomeDir(userHomeDirTextBox.Text.Trim());
            }

            OkButton.Enabled = validationLabel.Text.Length == 0;
        }

        public UserForm(Dictionary<string, User> users, Mode mode) {
            _usersDictionary = users;
            _mode = mode;

            InitializeComponent();
            Icon = Resources.app2;
            userHomeDirTextBox.ReadOnly = true;

            switch (_mode) {
                case Mode.Add:
                    Text = @"Add user";

                    ValidateFields();
                    break;
                case Mode.Edit:
                    Text = @"Edit user";

                    break;
                case Mode.Delete:
                    Text = @"Delete user";
                    userNameTextBox.ReadOnly = true;
                    userPasswordTextBox.ReadOnly = true;
                    userHomeDirTextBox.ReadOnly = true;
                    selectHomeDirButton.Enabled = false;

                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            userNameTextBox.TextChanged += (sender, args) => {
                ValidateFields();
            };
        }

        public override sealed string Text {
            get { return base.Text; }
            set { base.Text = value; }
        }

        public void Fill(User userData) {
            _userData = new User {
                Username = userData.Username, HomeDir = userData.HomeDir, Password = userData.Password
            };

            userNameTextBox.Text = userData.Username;
            userPasswordTextBox.Text = userData.Password;
            userHomeDirTextBox.Text = userData.HomeDir;

            ValidateFields();
        }

        public User Collect() {
            return new User {
                Username = userNameTextBox.Text.Trim(), Password = userPasswordTextBox.Text, HomeDir = userHomeDirTextBox.Text.Trim()
            };
        }

        private void OkButton_Click(object sender, EventArgs e) {
        }

        private void selectHomeDirButton_Click(object sender, EventArgs e) {
            var dialog = new FolderBrowserDialog();

            if (ValidateHomeDir(userHomeDirTextBox.Text.Trim()).Equals(string.Empty)) {
                dialog.SelectedPath = userHomeDirTextBox.Text.Trim();
            }

            if (dialog.ShowDialog() == DialogResult.OK) {
                userHomeDirTextBox.Text = dialog.SelectedPath;
            }

            ValidateFields();
        }
    }
}
