﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using FtpServiceGui.Properties;

namespace FtpServiceGui
{
    public partial class UserManagementForm : Form
    {
        private readonly Dictionary<string, User> _usersDictionary = new Dictionary<string, User>();

        public UserManagementForm()
        {
            InitializeComponent();
            Icon = Resources.app2;

            usersListView.MouseDoubleClick += (sender, args) => {
                ShowEditForm();
            };
        }

        public void Fill(List<User> users) {
            usersListView.Items.Clear();
            _usersDictionary.Clear();
            usersListView.BeginUpdate();
            users.ForEach(AddUser);
            usersListView.EndUpdate();
        }

        public List<User> Collect() {
            return _usersDictionary.Values.OrderBy(user => user.Username).ToList();
        }

        private void AddUser(User user) {
            if (_usersDictionary.ContainsKey(user.Username)) {
                return;
            }

            _usersDictionary[user.Username] = user;

            var el = new ListViewItem(user.Username);

            el.SubItems.Add(user.HomeDir);
            usersListView.Items.Add(el);

            /*
            foreach (ListViewItem item in usersListView.SelectedItems) {
                item.Selected = false;
            }

            el.Selected = true;
            usersListView.Select();*/
        }

        private ListViewItem FindByUser(User user) {
            return usersListView.Items.Cast<ListViewItem>().FirstOrDefault(item => item.Text.Equals(user.Username));
        }

        private void DeleteUser(User user) {
            _usersDictionary.Remove(user.Username);
            usersListView.Items.RemoveByKey(user.Username);
        }

        private void EditUser(User oldUser, User newUser) {
            if (oldUser.Equals(newUser)) {
                return;
            }

            var item = FindByUser(oldUser);

            if (item == null) {
                return;
            }

            DeleteUser(oldUser);
            AddUser(newUser);
            FindByUser(newUser).Selected = true;
            usersListView.Select();
        }

        private void addToolStripButton_Click(object sender, System.EventArgs e)
        {
            var addUserForm = new UserForm(_usersDictionary, UserForm.Mode.Add);

            if (addUserForm.ShowDialog() != DialogResult.OK) {
                return;
            }

            AddUser(addUserForm.Collect());
        }

        private void ShowEditForm() {
            var editUserForm = new UserForm(_usersDictionary, UserForm.Mode.Edit);
            var selectedUserName = usersListView.SelectedItems[0].SubItems[0].Text;
            editUserForm.Fill(_usersDictionary[selectedUserName]);

            if (editUserForm.ShowDialog() == DialogResult.OK) {
                EditUser(_usersDictionary[selectedUserName], editUserForm.Collect());
            }
        }

        private void editToolStripButton_Click(object sender, System.EventArgs e) {
            if (usersListView.SelectedItems.Count == 0) {
                return;
            }

            ShowEditForm();
        }

        private void deleteToolStripButton_Click(object sender, System.EventArgs e) {
            if (usersListView.SelectedItems.Count == 0) {
                return;
            }

            var deleteUserForm = new UserForm(_usersDictionary, UserForm.Mode.Delete);
            var selectedUserName = usersListView.SelectedItems[0].SubItems[0].Text;
            deleteUserForm.Fill(_usersDictionary[selectedUserName]);

            if (deleteUserForm.ShowDialog() == DialogResult.OK) {
                DeleteUser(_usersDictionary[selectedUserName]);
            }
        }
    }
}
